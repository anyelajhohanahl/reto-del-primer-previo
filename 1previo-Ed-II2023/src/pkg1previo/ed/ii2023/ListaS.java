/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package pkg1previo.ed.ii2023;

/**
 *
 * @author Docente
 */
public class ListaS<T> {

    private Nodo<T> cabeza;
    private int size;

    public ListaS() {
        this.cabeza = null; //no es necesario
        this.size = 0;
    }

    public void insertarInicio(T info) {
        Nodo<T> nuevo = new Nodo(info, this.cabeza);
        this.cabeza = nuevo;
        this.size++;

    }

    public void insertarFin(T info) {
        if (this.isEmpty()) {
            this.insertarInicio(info);
        } else {
            Nodo<T> x = getPos(this.size - 1); //mètodo encuentra la direcciòn de memoria del nodo que està de ùltimo
            Nodo<T> nuevo = new Nodo(info, null);
            x.setSig(nuevo);
            this.size++;
        }
    }

    public T get(int i) {
        return this.getPos(i).getInfo();
    }

    public void set(int i, T info) {
        this.getPos(i).setInfo(info);
    }

    private Nodo<T> getPos(int pos) {
        if (this.isEmpty()) {
            throw new RuntimeException("Lista vacìa, no hay posiciones");
        }
        if (pos < 0 || pos >= this.size) {
            throw new RuntimeException("Posición fuera de rango, esta debe ser entre:0 y " + (this.size - 1));
        }

        Nodo<T> temp = this.cabeza;
        //int i=0;
        while (pos > 0) //i<pos
        {
            temp = temp.getSig();
            pos--; //i++
        }
        return temp;
    }

    @Override
    public String toString() {
        String msg = "";
        for (Nodo<T> x = this.cabeza; x != null; x = x.getSig()) {
            msg += x.toString() + "->";
        }
        return "cab->" + msg + "null\n Contiene:" + this.size + " elementos";
    }

    public int getSize() {
        return size;
    }

    public boolean isEmpty() {
        return this.size == 0;
    }

    public void insertarOrdenado(T info) {
        if (this.isEmpty()) {
            this.insertarInicio(info);
        } else {
            Nodo<T> x = this.cabeza;
            Nodo<T> y = x;
            while (x != null) {
                Comparable comparador = (Comparable) info;
                int rta = comparador.compareTo(x.getInfo());
                if (rta < 0) {
                    break;
                }
                y = x;
                x = x.getSig();
            }
            if (x == y) {
                this.insertarInicio(info);
            } else {
                y.setSig(new Nodo(info, x));
                this.size++;
            }
        }
    }

    /**
     * metodo para eliminar alguna lista
     *
     */
    public void eliminarLista() {
        this.cabeza = null;
        this.size = 0;
    }

    public T remove(int index) {
        Nodo<T> x = null;
        Nodo<T> y = null;

        if (this.isEmpty()) {
            throw new RuntimeException("imposible borrar lista vacia");
        }

        if (index < 0 || index >= this.getSize()) {
            throw new RuntimeException("fuera de rango");
        }

        if (index == 0) {
            x = this.cabeza;
            this.cabeza = cabeza.getSig();

        } else {
            y = this.getPos(index - 1);
            x = y.getSig();
            y.setSig(x.getSig());

        }
        this.size -= 1;
        x.setSig(null);
        return x.getInfo();

    }

    public boolean contains(T info) {

        Nodo<T> current = cabeza;

        while (current != null) {
            Comparable comparador = (Comparable) info;
            if (comparador.compareTo(current.getInfo()) == 0) {
                return true; // Elemento encontrado
            }
            current = current.getSig();
        }

        return false; // Elemento no encontrado

    }

    /**
     * **
     * Método del previo1_punto 2
     *
     */
    public void virus(int n) {
        if (n < 0) {
            throw new RuntimeException("no se puede replicar -n veces ");
        }

        Nodo<T> actual = this.cabeza;
        Nodo<T> siguiente = actual.getSig();
        Nodo<T> menor = null;

        //   buscamos el menor
        while (siguiente != null) {
            Comparable comparador = (Comparable) actual.getInfo();
            int rta = comparador.compareTo(siguiente.getInfo());
            // T aux = actual.getInfo();
            if (rta < 1) {

                menor = actual;

            }
            actual = siguiente;
            siguiente = siguiente.getSig();
        }

        //agrgamos los repetidos 
    }

    /**
     * **
     * Método del challenge
     *
     */
    public void virus2(int n) {
        if (n < 0 || this.isEmpty()) {
            throw new RuntimeException("no se puede continuar con el virus");
        }

        Nodo<T> mayor = getMayor();
        ListaS<T> l = crear(mayor.getInfo(), n);

        int repMayor = repeticionMayor();

        Nodo<T> actual = cabeza;
        Nodo<T> siguiente = cabeza.getSig();
        Nodo<T> anterior = null;

        while (repMayor > 0 && actual != null) {

            if (actual.getInfo().equals(mayor.getInfo())) {
                if (anterior == null) {
                    // El elemento mayor es el primero de la lista
                    l.getPos(l.getSize() - 1).setSig(actual);
                    cabeza = l.cabeza;
                    this.size += l.size;
                } else {
                    anterior.setSig(l.cabeza);
                    l.getPos(l.getSize() - 1).setSig(actual);
                    this.size += l.size;
                }
                repMayor--;
            }
            anterior = actual;
            actual = siguiente;
            if (siguiente != null) {
                siguiente = siguiente.getSig();
            }
        }

    }

    public Nodo<T> getMayor() {
        if (isEmpty()) {
            throw new RuntimeException("La lista está vacía, no se puede encontrar el elemento mayor.");
        }

        Nodo<T> actual = cabeza;
        Nodo<T> mayor = cabeza;

        while (actual != null) {
            Comparable comparador = (Comparable) actual.getInfo();
            if (comparador.compareTo(mayor.getInfo()) > 0) {
                mayor = actual;
            }
            actual = actual.getSig();
        }

        return mayor;

    }

    private ListaS<T> crear(T info, int n) {
        ListaS<T> l = new ListaS();
        while (n-- > 0) {
            l.insertarFin(info);
        }
        return l;

    }

    private int repeticionMayor() {
        if (isEmpty()) {
            throw new RuntimeException("La lista está vacía, no se pueden contar repeticiones.");
        }

        Nodo<T> mayor = getMayor();
        int contador = 0;

        Nodo<T> actual = cabeza;

        while (actual != null) {
            Comparable comparador = (Comparable) actual.getInfo();
            if (comparador.compareTo(mayor.getInfo()) == 0) {
                contador++;
            }
            actual = actual.getSig();
        }

        return contador;
    }

}
