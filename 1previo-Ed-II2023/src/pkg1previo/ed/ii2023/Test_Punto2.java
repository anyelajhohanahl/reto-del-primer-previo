/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package pkg1previo.ed.ii2023;

/**
 *
 * @author Anyela Herrera
 */
public class Test_Punto2 {

    public static void main(String[] args) {
    System.out.println ("Vista para el punto2");
    //escenario A
    ListaS < Integer > La = new ListaS ();
    La.insertarFin (-9);
    La.insertarFin (7);
    La.insertarFin (2);
    La.insertarFin (3);
    La.insertarFin (0);
    System.out.println ("Imprimiendo lista:" + La.toString ());
    La.virus2(3);
    System.out.println ("Imprimiendo la:" + La.toString ());

    //escenario B
    ListaS < Integer > Lb = new ListaS ();
    Lb.insertarFin (7);
    Lb.insertarFin (2);
    Lb.insertarFin (-13);
    Lb.insertarFin (0);
    System.out.println ("Imprimiendo lista:" + Lb.toString ());
    Lb.virus2(2);
    System.out.println ("Imprimiendo lb:" + Lb.toString ());
    
    //escenario C
    try {
    ListaS < Integer > Lc = new ListaS ();
    Lc.insertarFin (-9);
    Lc.insertarFin (7);
    Lc.insertarFin (12);
    Lc.insertarFin (3);
    Lc.insertarFin (0);
    System.out.println ("Imprimiendo lista:" + Lc.toString ());
    Lc.virus2(-2);
    System.out.println ("Imprimiendo lc:" + Lc.toString());
    } catch(RuntimeException e) {
		    System.out.println(e.getMessage());
		}
    
    //escenario D
    ListaS < Integer > Ld = new ListaS ();
    Ld.insertarFin (-9);
    
    System.out.println ("Imprimiendo lista:" + Ld.toString ());
    Ld.virus2(4);
    System.out.println ("Imprimiendo ld:" + Ld.toString());

    // escenario E (con elementos repetidos)
    ListaS < Integer > Le = new ListaS ();
    Le.insertarFin (7);
    Le.insertarFin (2);
    Le.insertarFin (7);
    Le.insertarFin (0);
    System.out.println ("Imprimiendo lista:" + Le.toString ());
    Le.virus2(2);
    System.out.println ("Imprimiendo le:" + Le.toString ());
    
    // escenario F (con elementos repetidos)
    ListaS < Integer > Lf = new ListaS ();
    Lf.insertarFin (7);
    Lf.insertarFin (7);
    Lf.insertarFin (7);
    Lf.insertarFin (0);
    System.out.println ("Imprimiendo lista:" + Lf.toString ());
    Lf.virus2(2);
    System.out.println ("Imprimiendo lf:" + Lf.toString ());
  }
    
}
